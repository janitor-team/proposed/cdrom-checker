# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of Debian Installer templates to Esperanto.
# Copyright (C) 2005-2013 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Samuel Gimeno <sgimeno@gmail.com>, 2005.
# Serge Leblanc <serge.leblanc@wanadoo.fr>, 2005, 2006, 2007.
# Felipe Castro <fefcas@gmail.com>, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2017, 2018, 2020.
#
# Translations from iso-codes:
# Alastair McKInstry <mckinstry@computer.org>, 2001,2002.
# Copyright (C) 2001,2002,2003,2004 Free Software Foundation, Inc.
# D. Dale Gulledge <dsplat@rochester.rr.com> (translations from drakfw), 2001.
# Edmund GRIMLEY EVANS <edmundo@rano.org>, 2004-2011
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: cdrom-checker@packages.debian.org\n"
"POT-Creation-Date: 2019-09-19 16:20+0000\n"
"PO-Revision-Date: 2020-01-03 17:56+0000\n"
"Last-Translator: Felipe Castro <fefcas@gmail.com>\n"
"Language-Team: Esperanto <debian-l10n-esperanto@lists.debian.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: boolean
#. Description
#. :sl3:
#: ../cdrom-checker.templates:1001
msgid "Check integrity of the installation media?"
msgstr "Ĉu kontroli la integrecon de la instala aŭdovidaĵo?"

#. Type: boolean
#. Description
#. :sl3:
#: ../cdrom-checker.templates:1001
msgid "Warning: this check depends on your hardware and may take some time."
msgstr ""
"Atentu: tiu ĉi kontrolo dependas de via aparataro kaj eble longtempe daŭros."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-checker.templates:2001
msgid "Insert Debian installation media"
msgstr "Enŝovu Debianan instalan aŭdovidaĵon"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-checker.templates:2001
msgid ""
"Please insert one of the official Debian installation media before "
"continuing."
msgstr ""
"Bonvolu enŝovi unu el la oficialaj Debianaj instalaj aŭdovidaĵoj antaŭ ol "
"daŭrigi."

#. Type: error
#. Description
#. :sl3:
#: ../cdrom-checker.templates:3001
msgid "Failed to mount installation media"
msgstr "Munto de instala aŭdovidaĵo malsukcesis"

#. Type: error
#. Description
#. :sl3:
#: ../cdrom-checker.templates:3001
msgid ""
"The device '${CDROM}' could not be mounted correctly. Please check the media "
"and cables, and try it again."
msgstr ""
"La adaptilo '${CDROM}' ne povis esti korekte muntita. Bonvolu kontroli la "
"aŭdovidaĵon kaj kablojn kaj reprovu."

#. Type: error
#. Description
#. :sl3:
#: ../cdrom-checker.templates:4001
msgid "No valid Debian installation media"
msgstr "Neniu valida Debiana instala aŭdovidaĵo"

#. Type: error
#. Description
#. :sl3:
#: ../cdrom-checker.templates:4001
msgid ""
"What you have inserted is no valid Debian installation media. Please try "
"again."
msgstr ""
"Tio, kion vi enŝovis, ne estas valida Debiana instala aŭdovidaĵo. Bonvolu "
"reprovi."

#. Type: error
#. Description
#. TRANSLATORS: MD5 is a file name, don't translate it.
#. :sl3:
#: ../cdrom-checker.templates:5001
msgid "Failed to open checksum file"
msgstr "Malfermo de la suma kontroldosiero malsukcesis"

#. Type: error
#. Description
#. TRANSLATORS: MD5 is a file name, don't translate it.
#. :sl3:
#: ../cdrom-checker.templates:5001
msgid ""
"Opening the MD5 file on the media failed. This file contains the checksums "
"of the files located on the media."
msgstr ""
"Malfermo de dosiero MD5 en la aŭdovidaĵo malsukcesis. Tiu dosiero inkluzivas "
"la sumajn kontrolojn de ĉeestantaj dosieroj sur la aŭdovidaĵo."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-checker.templates:6001
msgid "Integrity test successful"
msgstr "Integreca kontrolo sukcesis"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-checker.templates:6001
msgid "The integrity test was successful. The installation image is valid."
msgstr "La integreca kontrolo sukcesis. La instalkopio validas."

#. Type: error
#. Description
#. :sl3:
#: ../cdrom-checker.templates:7001
msgid "Integrity test failed"
msgstr "Integreca testo mulsukcesis"

#. Type: error
#. Description
#. :sl3:
#: ../cdrom-checker.templates:7001
msgid ""
"The file ${FILE} failed the MD5 checksum verification. Your installation "
"media or this file may have been corrupted."
msgstr ""
"La dosiero ${FILE} malsukcesis dum kontrolo de sumo MD5. Eble la tuta "
"instala aŭdovidaĵo aŭ tiu ĉi dosiero estas difekta."

#. Type: boolean
#. Description
#. :sl3:
#: ../cdrom-checker.templates:8001
msgid "Check the integrity of another installation image?"
msgstr "Ĉu kontroli la integrecon de alia instalkopio?"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-checker.templates:9001
msgid "Insert Debian boot media"
msgstr "Enŝovu Debianan praŝargan aŭdovidaĵon"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-checker.templates:9001
msgid ""
"Please make sure you have inserted the Debian installation media you booted "
"from, to continue with the installation."
msgstr ""
"Bonvolu certigi ke vi enŝovis la Debianan instalan aŭdovidaĵon per kiu vi "
"praŝargis, por daŭrigi la instaladon."

#. Type: text
#. Description
#. :sl3:
#: ../cdrom-checker.templates:10001
msgid "Checking integrity of installation image"
msgstr "Kontrolo de integreco de la instalkopio"

#. Type: text
#. Description
#. :sl3:
#: ../cdrom-checker.templates:11001
msgid "Checking file: ${FILE}"
msgstr "Kontrolado de dosiero: ${FILE}"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep it under 65 columns
#. :sl2:
#: ../cdrom-checker.templates:12001
msgid "Check the integrity of installation media"
msgstr "Kontroli la integrecon de la instala aŭdovidaĵo"
